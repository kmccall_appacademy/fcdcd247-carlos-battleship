class Board
  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    if pos.nil? then !@grid.flatten.include?(:s) else @grid[pos[0]][pos[1]].nil? end
  end

  def full?
    @grid.flatten.count(nil) == 0
  end

  def place_random_ship
    raise if self.full?
    empties = []
    @grid.length.times do |i|
      @grid[i].length.times do |j|
        empties << [i, j] if @grid[i][j].nil?
      end
    end
    choice = empties.sample
    @grid[choice[0]][choice[1]] = :s
  end

  def won?
    @grid.flatten.count(:s) == 0
  end

end
